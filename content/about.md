---
title: "About"
subtitle: "Get to know us a bit more"
---

{{< carditem "Who are we?" "account-group" "" "A Free and Open Source development company, started in 2019" >}}
{{< carditem "Located in" "map-marker" "" "Wellington, New Zealand" >}}
{{< carditem "Developed on" "code-braces" "https://gitlab.com/flattrack/flattrack" "GitLab" >}}
{{< carditem "Contact found" "account-group" "/contact" "Here" >}}
