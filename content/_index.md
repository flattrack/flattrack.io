---
title: "FlatTrack"

features:
- name: Shopping List
  icon: format-list-checks
  shortDescription: Collaborate on groceries
  description: Add what you need to your list. Template a new list from last week's list. Find the total price of the whole list.
  unavailable: false
  opacity: 1
- name: Costs
  icon: currency-usd
  shortDescription: Keep track of spending
  description: Add one-off and recurring costs. Gauge spending for across year. Project how the year's spending may look.
  unavailable: true
  opacity: 0.9
- name: Tasks
  icon: clipboard-list
  shortDescription: Keep track of chores and other tasks
  description: Add your daily, weekly, and monthly tasks for your flat. Receive notifications for when you need them done. Automatically rotate between your flatmates.
  unavailable: true
  opacity: 0.8
- name: Board
  icon: view-dashboard-outline
  shortDescription: Share ideas and updates
  description: Post photos, updates, ideas, links, and much more.
  unavailable: true
  opacity: 0.6
- name: Shared Calendar
  icon: calendar
  shortDescription: Sync calendar events
  description: Keep up-to-date with events that are happening with your flatmates. Integrates with your existing calendar.
  unavailable: true
  opacity: 0.4
- name: Contacts
  icon: account-group
  shortDescription: Sync your flatmates accounts as contacts
  description: Get up-to-date contact information for your flatmates. Integrates with your existing contacts app.
  unavailable: true
  opacity: 0.2
- name: Recipes
  icon: food
  shortDescription: Create and share recipes
  description: Save recipes to share in the flat. Make it easy for everyone to have the meals that they love, made by anyone.
  unavailable: true
  opacity: 0.1
- name: High Fives
  icon: hand
  shortDescription: Celebrate your flatmates
  description: Send a positive message to a flatmate to say thanks for something.
  unavailable: true
  opacity: 0.05
---

