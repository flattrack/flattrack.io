#!/usr/bin/env bash

# Converts and rewrites references to images to the .webp format

set -o errexit
set -o nounset
set -o pipefail

for IMG in $(find . \
    -type f \
    -name '*.png' -or \
    -name '*.jpg' -or \
    -name '*.jpeg' -or \
    -name '*.tiff' -or \
    -name '*.PNG' -or \
    -name '*.JPG' -or \
    -name '*.JPEG' -or \
    -name '*.TIFF' \
    -not -path './.git/*' \
    -not -path 'public/*' \
    | sort | uniq); do \
    if [ ! -f "$IMG" ] || [ -z "$IMG" ]; then
        echo "error: file '$IMG' not found" > /dev/stderr;
        exit 1
    fi;
    echo "${IMG}"
    OUTPUT="${IMG%.*}.webp";
    if [ -f "$OUTPUT" ]; then
        echo "error: output file '$OUTPUT' already exists" > /dev/stderr;
        exit 1
    fi;
    cwebp -q "80" "$IMG" -o "$OUTPUT" || exit $?;
    rm "$IMG";
    echo "replaced '$IMG' with '$OUTPUT'"

    find . \
        -type f \
        -name '*.md' -or \
        -name '*.org' -or \
        -name '*.html' \
        -not -path './.git/*' \
        | xargs sed -i "s,${IMG#./static},${OUTPUT#./static},g"
done
