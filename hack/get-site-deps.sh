#!/bin/bash

set -o errexit
set -o nounset
set -o pipefail

cd $(git rev-parse --show-toplevel)
REPO_DIR="$PWD"

OUTPUT_FOLDER="$REPO_DIR/static/scripts/ext"

rm -rf "${OUTPUT_FOLDER}"
mkdir -p "${OUTPUT_FOLDER}"

ITEMS=(
    https://cdn.jsdelivr.net/npm/dayjs@1.11.10/dayjs.min.js
    https://cdn.jsdelivr.net/npm/dayjs@1.11.10/plugin/timezone.js
    https://cdn.jsdelivr.net/npm/dayjs@1.11.10/plugin/utc.js
)

cd "${OUTPUT_FOLDER}"
for SCRIPT in ${ITEMS[*]}; do
    curl -O -L -s "${SCRIPT}"
done

OUTPUT_FOLDER="$REPO_DIR/static/css/ext"

rm -rf "${OUTPUT_FOLDER}"
mkdir -p "${OUTPUT_FOLDER}"

ITEMS=(
    https://cdn.jsdelivr.net/npm/bulma@1.0.1/css/bulma.min.css
    https://github.com/Templarian/MaterialDesign-Webfont/raw/v7.4.47/css/materialdesignicons.min.css
)

cd "${OUTPUT_FOLDER}"
for SCRIPT in ${ITEMS[*]}; do
    curl -O -L -s "${SCRIPT}"
done

OUTPUT_FOLDER="$REPO_DIR/static/css/fonts"

rm -rf "${OUTPUT_FOLDER}"
mkdir -p "${OUTPUT_FOLDER}"

ITEMS=(
    https://github.com/Templarian/MaterialDesign-Webfont/raw/v7.4.47/fonts/materialdesignicons-webfont.eot?v=7.4.47
    https://github.com/Templarian/MaterialDesign-Webfont/raw/v7.4.47/fonts/materialdesignicons-webfont.woff?v=7.4.47
    https://github.com/Templarian/MaterialDesign-Webfont/raw/v7.4.47/fonts/materialdesignicons-webfont.ttf?v=7.4.47
)

cd "${OUTPUT_FOLDER}"
for SCRIPT in ${ITEMS[*]}; do
    curl -O -L -s "${SCRIPT}"
done
